import React from 'react';
import Container from '@material-ui/core/Container'
import { Typography, TextField, Button } from '@material-ui/core';

// Login Component

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        const username = this.props.username;
        alert('Username: ' + username + '\nPassword: ' + this.state.password);
        event.preventDefault();
        this.props.onLogin();
    }

    handleLogout() {
        this.props.onLogout();
        this.setState({password: ''});
    }

    render() {
        const loggedIn = this.props.loggedIn;
        const username = this.props.username;
        if (!loggedIn) {
            return (
                <Container component="main" maxWidth="xs">
                    <Typography component="h1" variant="h5">
                        Sign In
                    </Typography>
                    <form onSubmit={this.handleSubmit}>
                        <TextField
                            name="username"
                            label="Email Address"
                            required
                            variant="outlined"
                            margin="normal"
                            id="email"
                            autoComplete="email"
                            autoFocus
                            fullWidth
                            onChange={this.props.onUsernameChange}
                            value={username}
                        />
                        <TextField
                            name="password"
                            label="Password"
                            variant="outlined"
                            fullWidth
                            required
                            margin="normal"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={this.handleChange}
                            value={this.state.password}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                        >
                            Submit
                        </Button>
                    </form>
                </Container>
            );
        } else {
            return(
                <div>
                    <h1>You're logged in.</h1>
                    <Button 
                        onClick={this.props.onLogout} 
                        color="primary"
                        type="submit"
                        variant="contained"
                    >
                        Sign Out
                    </Button>
                </div>
            );
        }
    }
}

export default LoginForm;
