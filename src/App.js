import React from 'react';
import LoginForm from './LoginForm';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export default class App extends React.Component {
  constructor(props) {
    super(props)

    const name = localStorage.getItem('name');
    if (name) {
      this.state = {
        loggedIn: true,
        username: name
      }
    } else {
      this.state = {
        loggedIn: false,
        username: ''
      }
    }

    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
  }

  handleLogin() {
    this.setState({loggedIn: true});
    localStorage.setItem('name', this.state.username);
  }

  handleLogout() {
    this.setState({
      loggedIn: false,
      username: ''
    });
    localStorage.removeItem('name');
  }

  handleUsernameChange(e) {
    this.setState({username: e.target.value});
  }

  componentDidMount() {

  }

  render () {
    return (
      <Router>
        <div>
          <Header />

          <Route 
            exact path="/"
            render = {
              props => <Home 
                {...props}
                loggedIn={this.state.loggedIn}
                username={this.state.username}
              />
            }
          />
          <Route 
            path="/login"
            render={
              props => <LoginForm 
                {...props} 
                loggedIn={this.state.loggedIn}
                username={this.state.username}
                onLogin={this.handleLogin}
                onLogout={this.handleLogout}
                onUsernameChange={this.handleUsernameChange}
              />
            }
          />
        </div>
      </Router>
    );
  }
}

function Home(props) {
  return (
    <div>
      <h1>Home</h1>
      <Greeting loggedIn={props.loggedIn} name={props.username} />
    </div>
  );
}

function Header() {
  return (
    <ul>
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/login">Login</Link>
      </li>
    </ul>
  );
}

function Greeting(props) {
  const isLoggedin = props.loggedIn;
  const name = props.name;
  if (isLoggedin) {
    return (
      <h3>Hello {name}</h3>
    );
  } else {
    return(
      <h3>Please Login</h3>
    );
  }
}
